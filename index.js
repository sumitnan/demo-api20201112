const express = require('express')
const app = express()
let port = process.env.PORT || 8080;

const base_url = 'https://dummy-api.cm.edu/'

// --------------- INDEX ---------------//
// get_all_employee
// get_employee_by_id
// add_new_employee
// edit_employee_data
// delete_employee_data
// validate_data
// ------------- END INDEX ---=---------//

function get_all_employee() {
    app.get(`${base_url}/employees`, function (req, res) {
        res.send(res)
    })
}

function get_employee_by_id(id) {
    app.get(`${base_url}/employees/:${id}`, function (req, res) {
        res.send(res)
    });
}

function add_new_employee(emp_data) {
    newData = {
        "header":'Content-Type: application/json',
        "data-raw": {
            "firstname": `${emp_data.firstname}`,
            "lastname": `${emp_data.lastname}`,
            "birthday": `${emp_data.birthday}`,
            "email": `${emp_data.email}`
        }
    }
    newData = validate_data(newData)

    app.post(`${base_url}/employees`, function(req, res) {
        res.send(newData);
    });
}

function edit_employee_data(new_emp_data) {
    let temp = {}
    app.get(`${base_url}/employees/:${new_emp_data.id}`, function (req, res) {
        temp = {
            "header":'Content-Type: application/json',
            "data-raw":{
                "firstname":`${new_emp_data.firstname}`,
                "lastname":`${new_emp_data.lastname}`,
                "birthday":`${new_emp_data.birthday}`,
                "email":`${new_emp_data.email}`,
            }
        }
    });
    temp = validate_data(temp);
    app.put(`${base_url}/employees/:${new_emp_data.id}`, function (req, res){
        res.send(temp)
    })
}

function delete_employee_data(id) {
    app.delete(`${base_url}/employees/:${id}`,function (req, res){
        req.send(res)
    })
}

function validate_data(emp_data){
    const email_valid = /\w+@\w+.(\w+.+)/
    const birthday_valid = /\d{4}-\d{2}-\d{2}/
    
    if(emp_data['data-raw']['email'].search(email_valid) !== -1) {
        return `Email ไม่ถูกต้อง`
    }
    if(emp_data['data-raw']['birthday'].search(birthday_valid) !== -1){
        return `birthday ไม่ถูกต้อง`
    }
}

app.listen(port, () => console.log(`Example app listening on port ${port}`))